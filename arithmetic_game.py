import os
import random

from datetime import datetime
from operator import attrgetter


MAX_POINTS = 10
MAX_ERRORS = 10
FILENAME = 'users_scores.txt'
list_of_users = []
session = True


class User(object): 
    """User class to store the username elapsed time, and errors."""

    def __init__(self, elapsed_time, errors, username):
        self.username = username
        self.elapsed_time = elapsed_time
        self.errors = errors


def get_users():
    """Get all the users and their scores from the text file and save it to a list of User class."""
    
    if os.path.isfile('./{}'.format(FILENAME)):
        read = open(FILENAME,'r')
        contents = read.readlines()
        read.close()

        for line in contents:
            temporary_list= line.strip('\n').split(',')
            user = User(
                temporary_list[0], 
                temporary_list[1], 
                temporary_list[2]
                )
            list_of_users.append(user)

        return list_of_users


def print_instructions():
    """Print the instructions of the game."""

    sentences = """
    \n\tWelcome to the arithmetic game!\n
    Don't worry, you will only add, subtract, multiply, and divide.
    The game will only end if you have reached 10 points or 10 errors.\n
    Have fun!
        """ 

    print sentences


def get_username(username="?"):
    """
    Get the username of the user.

    Returns:
        Username entered if the username contains alphanumeric characters or
        the function itself if the username doesn't contain alphanumeric characters.
    """

    to_print = """
    Enter your username to begin
    Username must contain letters and numbers only.
    Press [ENTER] to quit game: """

    while(not username.isalnum() 
            and username is not ""):
        username = raw_input(to_print)
        return get_username(username)

    return username


def get_index (user_list, username):
    """
    Get the index of the username who is playing from the list of Users.

    Returns:
        Integer value that denotes the index of the username from the list.
    """

    for index in xrange(len(user_list)):
        if user_list[index].username == username:
            return index

    return -1


def is_right(answer, result):
    """Check if the answer by the user is right with the result."""

    try:
        if(int(answer) == result):
            status = True
        else:
            status = False
    except Exception as e:
        status = False

    return status


def randomize_problem():
    """Operation of the next problem will depend on the randomizer below."""

    criteria = random.randint(1, 4)

    if (criteria == 1):
        status = add_subtract(1)
    elif (criteria == 2):
        status = add_subtract(2)
    elif (criteria == 3):
        status = multiplication()
    else:
        status = division()

    return status


def add_subtract(criteria):
    """
    Either add or subtract two numbers. 
    The operation will depend based on the random number generated 
    from randomize_problem() function.
    """

    first_number = random.randint(11,99)
    second_number = random.randint(11,99)

    if(criteria == 1):
        ans = raw_input(("{} + {} = ").format(first_number,second_number))
        status = is_right(ans, first_number+second_number)
    elif(criteria == 2):
        if(second_number > first_number):
            ans = raw_input(("{} - {} = ").format(second_number,first_number))
            status = is_right(ans, second_number-first_number)
        else:
            ans = raw_input(("{} - {} = ").format(first_number,second_number))
            status = is_right(ans, first_number-second_number) 


    return status


def multiplication():
    """Multiply two numbers."""

    first_number = random.randint(11,99)
    second_number = random.randint(2,9)
    ans = raw_input(("{} * {} = ").format(first_number,second_number))
    status = is_right(ans, first_number*second_number)

    return status


def division():
    """Divide two numbers."""

    dividend = random.randint(100,999)
    divisor = random.randint(2,9)

    if (dividend%divisor is not 0):
        remainder = dividend % divisor
        dividend = dividend - remainder 
    
    ans = raw_input(("{} / {} = ").format(dividend,divisor))
    status = is_right(ans, dividend/divisor)

    return status


def calculate_time(start_time, end_time):
    """
    Convert the time to minutes:seconds.

    Return:
        String value of converted time.
    """
    
    elapsed_time = end_time - start_time
    hours,minutes,seconds = str(elapsed_time).split(":")
    temp_seconds = str(int(round(float(seconds))))

    if len(temp_seconds) == 1:
        temp_seconds = '0' + temp_seconds 

    str_time = minutes + ":" + temp_seconds

    return str_time


def save_score(current_username, elapsed_time, errors):
    """Save the username with his or her's score to the list."""

    idx = get_index(list_of_users, current_username)

    if (idx == -1):
        player = User(
            elapsed_time, 
            str(errors), 
            current_username
            )
        list_of_users.append(player)

    elif (idx >= 0):
        temp_time = list_of_users[idx].elapsed_time #get the time saved in the list

        if temp_time > elapsed_time: 
            list_of_users[idx].elapsed_time = str(elapsed_time) #replace this round's score
            list_of_users[idx].errors = str(errors)
        elif temp_time == elapsed_time:
            if int(list_of_users[idx].errors) > errors:
                list_of_users[idx].errors = str(errors)   
                list_of_users[idx].elapsed_time = str(elapsed_time)


    return list_of_users


def save_to_txt(user_list): 
    """Save the list of users to the text file."""

    write = open(FILENAME, 'w+')
    for index in xrange(len(user_list)):

        write.writelines(user_list[index].elapsed_time + ','
            + user_list[index].errors + ','
            + user_list[index].username + '\n'
            )

    write.close() 


def print_top_ten(user_list):
    """Prints the top 10 users from the list."""

    user_list = sorted(user_list, 
        key=attrgetter('elapsed_time', 'errors', 'username'))

    print "\n\nTOP 10 PLAYERS:"

    if len(user_list) > 10:
        limit = 10
    else:
        limit = len(user_list)

    for index in xrange(limit):
        print """{}. Name: {}\tTime: {}\tErrors: {}""".format(
            index+1,
            user_list[index].username,
            user_list[index].elapsed_time,
            user_list[index].errors
            )


try:
    get_users()
    while(session):
        print_instructions()
        current_username = get_username()

        if(current_username != ""):
            start = raw_input("\nClick [ENTER] key to start: ")
            points = 0
            errors = 0
            print "*"*53 + "\nGame will now begin!"
            start_time = datetime.now()

            while (points < MAX_POINTS and errors < MAX_ERRORS):
                status = randomize_problem()
                if (status):
                    points = points + 1
                    print "Correct! {} more points then you win!".format(MAX_POINTS-points)
                else:
                    errors = errors + 1
                    print "Nice try. {} more errors then you lose.".format(MAX_ERRORS-errors)

            end_time = datetime.now()
            elapsed_time = calculate_time(start_time, end_time)

            if (points == MAX_POINTS):
                print """\nYou win!\nPoints: {points}\nErrors: {errors}\nTime: {time}
                """.format(points=points,errors=errors,time=elapsed_time)
                save_score(current_username, elapsed_time, errors)
            elif (errors == MAX_ERRORS):
                print """\nYou failed the challenge.\nPoints: {points}\nErrors: {errors}\nTime: {time}
                """.format(points=points,errors=errors,time=elapsed_time)
                
        else:
            session = False

    save_to_txt(list_of_users)
    print_top_ten(list_of_users)

except (KeyboardInterrupt, EOFError):
    print_top_ten(list_of_users)